function printStatus(code: string| number){
    if(typeof code === 'string'){
        console.log(`My Status code is ${code.toLocaleString()} ${typeof code}`)   
    }else {
        console.log(`My Status code is ${code} ${typeof code}`)
    }
}

printStatus(404);
printStatus("abz");