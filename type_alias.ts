type CarYear = number;
type CarType = string;
type CarModel = string;

type Car = {
    year: CarYear,
    type: CarType,
    model: CarModel,
}

const CarYear: CarYear = 2001;
const CarType: CarType = "Honda";
const CarModel: CarModel = "Corolla";

const car1: Car = {
    year: 2001,
    type: "Nissan",
    model: "XXX",
}

console.log(car1)